﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
// using UnityEditor;
#endif

public class SkyboxRenderer : MonoBehaviour
{

    // Use this for initialization
    IEnumerator Start()
    {
        string jpgUrl = "http://site.lkuich.com/images/da/box.jpg";
        string metaUrl = jpgUrl + ".meta";
        string path = "Spheres/box";

        var asset = new RemoteSkyboxRenderer().DownloadAssetBundle<Texture>("box", jpgUrl, 1);
        int i = 0;

        yield return null;
    }

    void SetSkybox(Material material)
    {
        GameObject camera = Camera.main.gameObject;
        Skybox skybox = camera.GetComponent<Skybox>();
        if (skybox == null)
            skybox = camera.AddComponent<Skybox>();
        skybox.material = material;
    }

    private IEnumerator DownloadSkybox(string jpgUrl, string metaUrl)
    {
        // Download JPG
        WWW jpg = new WWW(jpgUrl);
        yield return jpg;

        WWW meta = new WWW(metaUrl);
        yield return meta;

        // Instantiate(jpg.assetBundle.mainAsset);

        Texture2D texture = jpg.texture;
        // SetTextureImporterFormat(texture, true);

        byte[] bytes = texture.EncodeToJPG();
        // File.WriteAllBytes(path, bytes);

#if UNITY_EDITOR
        /*
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset(path);
            TextureImporter importer = AssetImporter.GetAtPath(path) as TextureImporter;
            importer.textureType = TextureImporterType.Cubemap;
            importer.generateCubemap = TextureImporterGenerateCubemap.Cylindrical;
            importer.SaveAndReimport();
            // AssetDatabase.Refresh();

            AssetDatabase.WriteImportSettingsIfDirty(path);
        */
#endif
    }
}

public class RemoteSkyboxRenderer
{
    public Object Obj;

    public IEnumerator DownloadAssetBundle<T>(string asset, string url, int version) where T : Object
    {
        Obj = null;

        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        // Start the download
        using (WWW www = WWW.LoadFromCacheOrDownload(url, version))
        {
            yield return www;
            if (www.error != null)
                throw new System.Exception("WWW download:" + www.error);
            AssetBundle assetBundle = www.assetBundle;
            Obj = assetBundle.LoadAsset(asset, typeof(T));
            // Unload the AssetBundles compressed contents to conserve memory
            // bundle.Unload(false);

        } // memory is freed from the web stream (www.Dispose() gets called implicitly)
    }
}